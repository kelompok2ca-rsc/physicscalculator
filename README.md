# TUGAS KELOMPOK RSC 1 : GITLAB

## Kelompok 2
- ### Jeremya Dharmawan Raharjo / 16521268
- ### Sinekar Lintang / 16521033
- ### Rizky Abdillah Rasyid / 16521049
<p>&nbsp;</p>

## DESKRIPSI TUGAS
<p>&nbsp;</p>

Membuat dummy repository di GitLab yang terdapat:
- Branch main, feature and  dev Branch (1 orang mengerjakan 1 feature branch)
- Merge dari branch fitur ke branch fitur lainnya
- Merge dari branch fitur ke dev branch
- Merge dari dev branch ke main

Bonus:
- Setup board : setup issue list dan board di GitLab 

### DESKRIPSI PEMBAGIAN BRANCH
- Main Branch
- dev Branch
- feature-GL Branch 
- feature-GM Branch
- feature-GP Branch