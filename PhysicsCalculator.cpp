# define _USE_MATH_DEFINES 

#include <iostream>
#include <cmath>

using namespace std;

class GerakLurus{
    // Attribute
    public:
        double X_init, V_init, Acceleration, Time;
    
    // deklarasikan konstruktor
    GerakLurus(double Xi, double Vi, double A, double t){
        X_init = Xi;
        V_init = Vi;
        Acceleration = A;
        Time = t;
    }
   

    double getDistance(){ // method 1: Mencari jarak tempuh
        return X_init + V_init * Time + 0.5 * Acceleration * pow(Time,2);
    }
    
    double getVelocity(){ // method 2: Mencari kecepatan sesaat
        return V_init + Acceleration * Time;
    }

    double avgVelocity(){ // method 3: Mencari kecepatan rata-rata
        return getDistance()/Time;
    }
        
};

class GerakMelingkar{
    // Attirubute
	public:
        double theta_i, omega_i, alpha, t;
    
    // constructor
    GerakMelingkar(double theta_i, double omega_i, double alpha, double t){
        GerakMelingkar::omega_i = omega_i;
        GerakMelingkar::theta_i = theta_i;
        GerakMelingkar::alpha = alpha;
        GerakMelingkar::t = t;
    }

    // Method 1 : Theta Akhir / Final
    double getTheta_f(){
        return theta_i + omega_i * t + 0.5 * alpha * t * t;
    }

    // Method 2 : Omega Akhir / Final
    double getOmega_f(){
        return omega_i + alpha * t;
    }

    // Method 3 : Periode 
    double getPeriod(){
		double omega_f;
		omega_f = omega_i + alpha * t;
		return 2 * 3.14 * omega_f;
	}

    // Method 4 : Frekuensi
    double getFrequency(){
		double omega_f;
		omega_f = omega_i + alpha * t;
		return 1 / (2 * 3.14 * omega_f);
	}
};

class GerakParabola{
    // attribute
    public :
        double v,t,theta;

    // constructor
    GerakParabola(double v, double t, double theta){
        GerakParabola::v = v;
        GerakParabola::t = t;
        GerakParabola::theta = theta;
    };

    // Method 1 : posisi x
    double GetDistanceX(){
        return v*t*cos(theta*M_PI/180);
    }
    // Method 2 : posisi y
    double GetDistanceY(){ 
        return (v*t*sin(theta*M_PI/180))-(0.5*9.8*t*t);
    }
    // Method 3 : Posisi X maks
    double Xmax(){
        return v*v*sin(2*theta*M_PI/180)/9.8;
    }
    // Method 4 : Posisi Y maks
    double Ymax(){
        return v*v*pow(sin(2*theta*M_PI/180),2)/(2*9.8);
    }
    // Method 5 : Kecepatan komponen X
    double GetVelocityX(){
        return v*cos(theta*M_PI/180);
    }
    // Method 6 : Kecepatan komponen Y
    double GetVelocityY(){
        return (v*sin(theta*M_PI/180))-(9.8*t);
    }

};

int main()
{   
    // Instance
    GerakLurus GL1 =  GerakLurus(2, 4, 6, 10);
    GerakMelingkar GM1 = GerakMelingkar(3, 7, -2, 10);
    GerakParabola GP1 = GerakParabola(20, 53, 10);

    // Test Class GerakMelingkar
    cout << "Gerak Melingkar 1" << endl;
    cout << "Posisi Sudut Akhir/ Theta_f : " << GM1.getTheta_f() << endl;
    cout << "Kecepatan Sudut Akhir/ Omega_f : " << GM1.getOmega_f() << endl;
    cout << "Periode : " << GM1.getPeriod() << endl;
    cout << "Frekuensi : " << GM1.getFrequency() << endl; 

    // Test GerakParabola
    cout << "Gerak Parabola" << endl;
    cout << "Jarak X = " << GP1.GetDistanceX() << endl;
    cout << "Jarak Y = " << GP1.GetDistanceY() << endl;
    cout << "X maksimum = " << GP1.Xmax() << endl;
    cout << "Y maksimum = " << GP1.Ymax() << endl;
    cout << "Kecepatan X = " << GP1.GetVelocityX() << endl;
    cout << "Kecepatan Y = " << GP1.GetVelocityY() << endl;
    
    // Test GerakLurus
    cout << "Gerak Lurus" << endl;
    cout << " Jarak tempuh: " << GL1.getDistance() << endl;
    cout << " Kecepatan sesaat: " << GL1.getVelocity() << endl;
    cout << "Kecepatan rata-rata: " << GL1.avgVelocity() << endl;
    return 0;
}

